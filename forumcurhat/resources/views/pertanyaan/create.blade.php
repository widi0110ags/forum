@extends('forum.master')


@section('content')
  <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title"> Pertanyaan</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/pertanyaan" method="POST">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="JudulPertanyaan">Judul Pertanyaan</label>
                    <input type="text" class="form-control" id="JudulPertanyaan" name="Judul" placeholder="Enter Judul">
                  </div>
                  <div class="form-group">
                    <label for="IsiPertanyaan">Isi Pertanyaan/label>
                    <input type="text" class="form-control" id="IsiPertanyaan" name="Isi" placeholder="Enter Pertanyaan">
                  </div>
                </div>
                <div class="card-body">
                <!-- Date range -->
                <div class="form-group">
                  <label>Tanggal Dibuat:</label>

                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="far fa-calendar-alt"></i>
                      </span>
                    </div>
                    <input type="text" class="form-control float-right" id="reservation">
                  </div>
                  <!-- /.input group -->
                </div>
                <!-- Date range -->
                <div class="form-group">
                  <label>Tanggal Diperbarui:</label>

                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="far fa-calendar-alt"></i>
                      </span>
                    </div>
                    <input type="text" class="form-control float-right" id="reservation">
                  </div>
                  <!-- /.input group -->
                </div>
                <!-- /.form group -->



                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
@endsection
