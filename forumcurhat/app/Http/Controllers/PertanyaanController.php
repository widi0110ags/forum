<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{

    public function index(){
      return view ('pertanyaan.index');
    }

    public function create(){
      return view ('pertanyaan.create');
    }

    public function store(Request $request){

      $request->validate([
        'judul' => 'required|unique:pertanyaan',
        'isi'=> 'required',
        'tanggal_dibuat' => 'required',
        'tanggal_diperbarui' => 'required'
      ]);
        $query = DB::table('pertanyaan')->insert([
          "judul" => $request ["judul"],
          "isi" => $request ["isi"],
          "tanggal_dibuat" => $request ["tanggal_dibuat"],
          "tanggal_diperbarui" => $request ["tanggal_diperbarui"]
        ]);
        return redirect('/pertanyaan');
    }
}
